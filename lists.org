* Lists

Are chain of values, a value connected a next value; a linked list.

Const cells; it is a pair of two values (tupples in other languages);

#+begin_src elisp
  (cons 1 2) ;; (1 . 2 ) this is a cons

  '(1 . 2) ;; (1 . 2)

  (car '(1 . 2)) ;; 1, because value at left position
  (cdr '(1 . 2)) ;; 2, because value at right position

  (setq some-cons '(1 . 2))

  (setcar some-cons 3)

  some-cons ;; (3 . 2)
#+end_src

Building lists from cons
#+begin_src elisp
  (cons 1 '(2 3 4))

  (cons 1 (cons 2 nil))

  (cons '(1 2 3) '(4))

  (append '(1 2 3) '(4))
#+end_src

* Predicates

Test if something is a list

#+begin_src elisp
  (listp '(1 2 3)) ;; t
  (listp nil) ;; t

  (listp 1) ;; nil


  (append '(1) nil nil '(2))

  (listp (cons 1 (cons 2 nil)))
#+end_src

* alists

Association lists, are lists containing cons paires for the purpose of storing named values.

#+begin_src elisp
  (setq some-alist '((one . 1)
		     (two . 2)
		     (three . 3)
		     (my-key . "sup")))

  (alist-get 'one some-alist) ;; 1

  (alist-get 'my-key some-alist)

  (assq 'one some-alist)

  (rassq 1 some-alist)

  (setf (alist-get 'one some-alist) 5)

  some-alist
#+end_src

* plists

#+begin_src elisp
  (plist-get '(one 1 two 2) 'one)

  (plist-put '(one 1 two 2) 'three 3)
#+end_src
