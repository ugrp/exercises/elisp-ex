* named functions
** defining functions
A function =do-some-math=, with two parameters: =x= =y=;

#+begin_src elisp
  (defun do-some-math (x y)
    (* (+ x 20)
       (- y 10)))

  (do-some-math 100 40)
#+end_src

** functions arguments

Optional arguments

#+begin_src elisp
  (defun multiply-maybe (x &optional y z)
    (* x
       (or y 1)
       (or z 1)))

  (multiply-maybe 5)
  (multiply-maybe 5 7)
  (multiply-maybe 5 9 88)

  (multiply-maybe 5 9 10 299 19) ;; wrong number of arguments
#+end_src

"Recursive" multiplication:
#+begin_src elisp
  (defun multiply-many (x &rest operands)
    (dolist (operand operands)
      (when operand
	(setq x (* x operand))
	))
    x)

  (multiply-many 1 2 3 4)
  (multiply-many 1 8 10 nil)
#+end_src

Multiply non-nil operands with an optional operand
#+begin_src elisp
  (defun multiply-two-or-many (x &optional y &rest operands)
    (setq x (* x (or y 1)))
    (dolist (operand operands)
      (when operand
	(setq x (* x operand))))
    x)

  (multiply-two-or-many 1)
  (multiply-two-or-many 1 10 9)
  (multiply-two-or-many 0 19 99) ;; 0
  (multiply-two-or-many 20 19 99)
#+end_src
* documenting functions

The first form in a function body can be a string, which describes the
function.

#+begin_src elisp
    ;; evaluate this expression, and use =M-x= =describe-function=
    (defun do-some-math (x y)
		  "Adds x to y.
  You can make a line-break? 
  You can do a longer string And this can you?"
		  (+ x y))

    (do-some-math 7 2)
#+end_src
* lambdas, anonymous functions (without name)


#+begin_src elisp
  ;; a lambda definition, not called
  (lambda (x y)
    (+ 100 x y))

  ;; a lambda that is called
  ((lambda (x y)
     (+ 100 x y))
   10 20)
#+end_src

Lambda, because of lambda calculus, inspiration for lisps

https://en.wikipedia.org/wiki/Lambda_calculus
* invoking functions

In =funcall=, the single quote ='=, means to quote the following, not
evaluating it.

#+begin_src elisp
  ;; default way of calling a function
  (+ 2 2) ;; 4

  ;; calling it by symbol, check fn definition in emacs
  (funcall '+ 3 3) ;; 6
#+end_src

Example of using functions as data;

#+begin_src elisp
  ;; defines a function that accepts a function
  (defun gimme-function (fun x)
    (message "function: %s -- Result: %d"
	     fun
	     (funcall fun x)))

  ;; store a lambda in a variable
  (setq function-in-variable (lambda (arg) (+ arg 1)))

  ;; define an equivalent function
  (defun named-version (arg)
    (+ arg 1))

  ;; invoque lambda from parameters
  (gimme-function (lambda (arg) (+ arg 1)) 5)

  ;; invoque lamdta stored in variable
  (gimme-function function-in-variable 5)

  ;; invoque function by passing symbol, quote ='= character
  (gimme-function 'named-version 5)
#+end_src


Other way to use function, =apply=;

#+begin_src elisp
  (apply '+ '(2 2))
  (funcall '+ 2 2)

  ;; even works with &optional and &rest parameters
  (apply 'multiply-many '(1 2 3 4 5 6 7 8 9))
#+end_src
* interactive functions = commands
Commands are "interactive functions", meant to be available in =M-x=.
#+begin_src elisp
  (defun my-first-command ()
    (interactive)
    (message "YOLO: moto"))
#+end_src

Example of an interactive function that takes arguments;
#+begin_src elisp
  ;; "N N" means, accept two "number" arguments, interactively
  (defun my-add (x y)
    (interactive "Nx:\nNy:")
    (message "Result: %d" (+ x y)))

  (global-set-key (kbd "C-c z") 'my-add)
#+end_src

| M | prompt for a string                                  |
| N | prompt for numbers or use a numeric prefix arguments |
| p | use numeric prefix without prompting                 |
| i | skip an "irrelevant" arg                             |
| F | prompt for a file, providing completion              |
| D | prompt for a directory, providing completion         |
| b | prompt for a buffer, with completion                 |
| C | prompt for a command name                            |
| a | prompt for a function name                           |
| v | prompt for a custom variable name                    |

An example using Directory prompt:
#+begin_src elisp
  ;; "D" in interactive arg string
  ;; means, accept one "directory" arguments
  (defun my-backup (dir-path)
    (interactive "DFolder to backup:")
    (message "Saved: %s" dir-path)

  (global-set-key (kbd "C-c z") 'my-backup)
#+end_src
* Ex: A .dotfiles manager

#+begin_src elisp
  (setq dotfiles-folder "./")
  (setq dotfiles-org-files '("init.org" "emacs.org"))

  (defun dotfiles-tangle-org-file (&optional org-file)
    (interactive "F")
    (org-babel-tangle-file (expand-file-name org-file dotfiles-folder)))

  (defun dotfiles-tangle-org-files ()
    (interactive)
    (dolist (org-file dotfiles-org-files)))
#+end_src
